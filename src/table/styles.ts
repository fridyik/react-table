import { makeStyles, Theme } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) => ({
    container: {
        display: 'flex',
        alignItems: 'center',
        backgroundColor: '#d5dde0',
        padding: 10,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5
    },
    paper: {
        borderRadius: 5
    },
    table: {
        minWidth: 750,
    },
    icon: {
        height: 40,
    },
    search: {
        paddingLeft: 10,
    },
    searchIcon: {
      color: 'grey'
    },
    cell: {
        display: 'flex',
    },
    count: {
        display: 'flex',
        alignItems: 'center',
        padding: 5,
        minWidth: 15,
    },
    avatar: {
        padding: 5,
    },
    textWrap: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        padding: 5,
    },
    mainText: {
        fontSize: 16,
        fontWeight: 600,
        color: 'black',
    },
    text: {
        fontSize: 12,
        color: 'grey',
    },
    medals: {
        display: 'flex', justifyContent: 'center',
    },
    even: {
        backgroundColor: 'white',
    },
    odd: {
        backgroundColor: 'rgb(245 242 240)',
    },
    hide: {
        display: 'none',
    },
    paginationRoot: {
        display: 'flex',
        justifyContent: 'center',
        height: 5
    },
    toolbar: {
        position: 'absolute'
    },
    rootTable: {
        minWidth: 500
    },
    actions: {
        marginLeft: 0,
        color: '#fafafa'
    },
    views: {
        fontWeight: 600,
        fontSize: 14
    }
}));
