import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Avatar from 'react-avatar';
import Typography from '@material-ui/core/Typography';
import { InputBase } from '@material-ui/core';
import { Search as SearchIcon } from '@material-ui/icons';
import { useStyles } from './styles';
import { rows, sortedViews } from './utils';
import { EnhancedTableHead } from './tableHead';
import { getComparator, stableSort } from './utils/sort';
import golden from './medals/golden.svg';
import silver from './medals/silver.svg';
import bronze from './medals/bronze.svg';


export default function EnhancedTable() {
    const classes = useStyles({});
    const [order, setOrder] = React.useState('desc');
    const [orderBy, setOrderBy] = React.useState('pageViews');
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);
    const [searchValue, setSearchValue] = React.useState('');

    const handleRequestSort = (event: any, property: React.SetStateAction<string>) => {
        const isAsc = orderBy === property && order === 'asc';
        setOrder(isAsc ? 'desc' : 'asc');
        setOrderBy(property);
    };


    const handleChangePage = (event: any, newPage: React.SetStateAction<number>) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event: { target: { value: string; }; }) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };


    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setPage(0);
        setSearchValue(event.target.value);
    };

    const filterNames = () => {
        return rows.filter((row: { name: string; countPub: number; pageViews: number; }) => {
            return row.name.includes(searchValue.trim());
        });

    };


    return (
        <Paper className={classes.paper}>
            <div className={classes.container}>
                <SearchIcon className={classes.searchIcon}/>
                <InputBase
                    className={classes.search}
                    placeholder={`Поиск авторов по имени`}
                    inputProps={{ 'aria-label': 'search ' }}
                    value={searchValue}
                    onChange={handleChange}
                />
            </div>
            <Table
                className={classes.table}
                aria-labelledby='tableTitle'
                size='small'
                aria-label='enhanced table'
                classes={{
                    root: classes.rootTable,
                }}
            >
                <EnhancedTableHead
                    classes={classes}
                    order={order}
                    orderBy={orderBy}
                    onRequestSort={handleRequestSort}
                    rowCount={rows.length}
                />
                <TableBody>
                    {stableSort(filterNames(), getComparator(order, orderBy))
                        .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                        .map((row: { name: string; countPub: number; pageViews: number; }, index: number) => {
                            const labelId = `enhanced-table-checkbox-${index}`;

                            return (
                                <TableRow
                                    hover
                                    role='checkbox'
                                    tabIndex={-1}
                                    key={row.name}
                                    className={index % 2 === 0 ? classes.even : classes.odd}
                                >
                                    <TableCell component='th' id={labelId} scope='row' padding='none'>
                                        <div className={classes.cell}>
                                            <div className={classes.count}>
                                                {page * rowsPerPage + index + 1}
                                            </div>
                                            <div className={classes.avatar}>
                                                <Avatar name={row.name} round={true} maxInitials={1} size='40'/>
                                            </div>
                                            <div className={classes.textWrap}>
                                                <Typography className={classes.mainText}>
                                                    {row.name}
                                                </Typography>
                                                <Typography
                                                    className={classes.text}> {row.countPub} публ.</Typography>
                                            </div>
                                        </div>
                                    </TableCell>
                                    <TableCell>
                                        <div className={classes.medals}>
                                            {row.pageViews === sortedViews[0] ?
                                                <img className={classes.icon} src={golden}
                                                     alt='Golden'/> : '' ||
                                                row.pageViews === sortedViews[1] ?
                                                    <img className={classes.icon} src={silver}
                                                         alt='silver'/> : '' ||
                                                    row.pageViews === sortedViews[2] ?
                                                        <img className={classes.icon} src={bronze}
                                                             alt='bronze'/> : ''}
                                        </div>
                                    </TableCell>
                                    <TableCell align='right'>
                                        <Typography className={classes.views}>
                                            {row.pageViews}
                                        </Typography>
                                    </TableCell>
                                </TableRow>
                            );
                        })}
                </TableBody>
            </Table>
            <TablePagination
                component='div'
                count={rows.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
                classes={{
                    root: classes.paginationRoot,
                    selectRoot: classes.hide,
                    caption: classes.hide,
                    toolbar: classes.toolbar,
                    actions: classes.actions,
                }}
            />
        </Paper>
    );
}
