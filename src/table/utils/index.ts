import data from '../data/data.json'

export const headCells = [
    { id: 'name', numeric: false, disablePadding: true, label: 'Name' },
    { id: 'medal', numeric: false, disablePadding: true, label: '' },
    { id: 'pageViews', numeric: true, disablePadding: false, label: 'pageViews' },
];

function createData(name: string, countPub: number, pageViews: number) {
    return { name, countPub, pageViews };
}

export const rows = data.map((jsonData) => createData(jsonData.name, jsonData.count_pub, jsonData.pageviews));

const medal = data.map((i) => i.pageviews);

export const sortedViews = medal.sort((a, b) => b - a);
