import React from 'react';
import './App.css';
import EnhancedTable from './table';

function App() {
    return (
        <div className='App-header'>
            <EnhancedTable/>
        </div>
    );
}
//#04445d
//#24648a

export default App;
